import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template
import sys

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger()

class spike(pluginTemplate):
    __model__ = "Spike"
    __version__ = "0.5.0"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)

        config = kwargs.get('config')
        if config is None:
            print("Please enter input file paths in configuration.")
            raise SystemExit
        else:
            self.isa_spec = os.path.abspath(config['ispec'])
            self.platform_spec = os.path.abspath(config['pspec'])

        return sclass

    def initialise(self, suite, work_dir, compliance_env):
        self.work_dir = work_dir
        self.compile_cmd = 'riscv32-unknown-elf-gcc -march={0} -mabi=ilp32 \
         -static -mcmodel=medany -fvisibility=hidden -nostdlib -nostartfiles\
         -T /home/neel/temp/riscof-demo/riscof-plugins/spike_simple/env/link.ld\
         -I /home/neel/temp/riscof-demo/riscof-plugins/spike_simple/env/\
         -I ' + compliance_env

    def build(self, isa_yaml, platform_yaml):
        ispec = utils.load_yaml(isa_yaml)
        self.isa = ispec["ISA"]

    def runTests(self, testList):
        for file in testList:
            testentry = testList[file]
            test = os.path.join(constants.root, str(file))
            test_dir = testentry['work_dir']

            elf = 'my.elf' 
                  
            cmd = self.compile_cmd.format(testentry['isa'].lower()) + ' ' + test + ' -o ' + elf
            compile_cmd = cmd + ' -D' + " -D".join(testentry['macros'])
            logger.debug('Compiling test: ' + test)
            utils.shellCommand(compile_cmd).run(cwd=test_dir)

            execute = 'spike --isa={0} +signature=sign {1}'.format(self.isa, elf)
            logger.debug('Executing on Spike ' + execute)
            utils.shellCommand(execute).run(cwd=test_dir)

            sign_fix = 'sh /home/neel/temp/riscof-demo/riscof-plugins/spike_simple/env/sign_fix.sh'
            utils.shellCommand(sign_fix).run(cwd=test_dir)

            rename_sign = 'cat sign > ' + os.path.join(test_dir, self.name[:-1] + ".signature")
            utils.shellCommand(rename_sign).run(cwd=test_dir)
