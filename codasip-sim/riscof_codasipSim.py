import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger()

map = {
    'rv32i': 'rv32i',
    'rv32im': 'rv32im',
    'rv32ic': 'rv32ic',
    'rv32ia': 'rv32ia'
}


class codasipSim(pluginTemplate):
    __model__ = "Codasip_Simulator"
    __version__ = "0.5.0"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)

        config = kwargs.get('config')
        if config is None:
            print("Please enter input file paths in configuration.")
            raise SystemExit
        else:
            self.isa_spec = config['ispec']
            self.platform_spec = config['pspec']

        compile_flags = ' -static -mcmodel=medany -fvisibility=hidden -nostdlib \
        -nostartfiles '

        self.pref = "codix_berkelium-ia-"
        self.gcc = "codix_berkelium-ia-" + 'gcc'
        self.root_dir = constants.root
        self.env_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                    "env")
        self.linker = os.path.join(self.env_dir, "link.ld")
        self.user_abi = "ilp32"
        self.user_target = "codasip_sim"
        self.objdump = "codix_berkelium-ia-" + 'objdump -D '
        self.compile_cmd = self.gcc+ ' -march={0} -mabi={1} '+compile_flags +\
                ' -T'+self.linker
        self.objdump = "codix_berkelium-ia-" + 'objdump -D'
        return sclass

    def initialise(self, suite, work_dir, env):
        self.suite = suite
        self.work_dir = work_dir
        self.compile_cmd = self.compile_cmd + " -I" + env + " -I" + self.env_dir

    def build(self, isa_yaml, platform_yaml):
        ispec = utils.load_yaml(isa_yaml)
        self.isa = ispec["ISA"]

    def simulate(self, file):
        test_dir = os.path.join(self.work_dir,
                                str(file.replace(self.suite, '')[:-2]))
        elf = os.path.join(test_dir, str(file.split("/")[-1][:-2]) + '.elf')
        sign_file = os.path.join(test_dir, self.name[:-1] + "_sign")
        output_file = os.path.join(test_dir, self.name[:-1] + "_out")
        d = dict(elf=elf,
                 testDir=test_dir,
                 isa=self.isa,
                 out=output_file,
                 sign=sign_file)

        logger.debug(self.name + "Simulate")
        command = Template(
            'codix_berkelium-ia-isimulator -r --info 5 ${elf} 2 > ${out} 1> ${sign}'
        ).safe_substitute(d)
        utils.shellCommand(command).run(cwd=test_dir)
        return sign_file

    def compile(self, file, isa, macros):
        logger.debug(self.name + "Compile")
        test = os.path.join(self.root_dir, str(file))
        test_dir = os.path.join(self.work_dir,
                                str(file.replace(self.suite, '')[:-2]))
        elf = os.path.join(test_dir, str(file.split("/")[-1][:-2]) + '.elf')
        cmd = self.compile_cmd.format(map[isa.lower()],
                                      self.user_abi) + ' ' + test + ' -o ' + elf
        execute = cmd + ' -D' + " -D".join(macros)
        utils.Command(execute).run(cwd=test_dir)
        cmd = self.objdump.format(test, self.user_abi) + ' ' + elf
        utils.shellCommand(
            cmd + ' > {}.disass'.format(str(file.split("/")[-1][:-2]))).run(
                cwd=test_dir)
