### Building C-class simulation executable

Make sure you have verilator (v4.000 and above), dtc-1.4.7 and BLUESPEC installed.
```
git clone git@gitlab.com:shaktiproject/cores/c-class.git
cd c-class/base-sim
./manager.sh update_deps
make 
```

This above will generate a `out` binary in the bin folder along with `boot.*` files.


### Riscof Plugin for shakti E-Class

- Config file entry
```
DUTPlugin=cClass

[cClass]
cClassBin=/path_to_cclass_bin
```

- Export command
```
pwd=pwd;export PYTHONPATH=$pwd:$PYTHONPATH;
```
