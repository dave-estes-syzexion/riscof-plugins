import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger()

map = {
    'rv32i': 'rv32i',
    'rv32im': 'rv32im',
    'rv32ic': 'rv32ic',
    'rv32ia': 'rv32ia'
}


class yamlPlugin(pluginTemplate):
    __model__ = "Yaml Plugin"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)
        config = kwargs.get('config')
        if config is None:
            env_file = input("Enter env file path for " + self.name)
        else:
            env_file = config['envFile']
        env_file = os.path.abspath(env_file)
        foo = utils.load_yaml(env_file)
        self.isa_spec = os.path.abspath(foo["ispec"])
        self.platform_spec = os.path.abspath(foo["pspec"])
        compile_flags = ' -static -mcmodel=medany -fvisibility=hidden -nostdlib \
        -nostartfiles '

        self.__model__ = foo['model']
        self.__version__ = foo['version']

        self.simulator = foo['USER_EXECUTABLE']
        self.signature = foo['USER_SIGN']
        self.pref = foo['RISCV_PREFIX']
        self.post = foo['USER_POST_SIM']['command']
        self.is_post_shell = foo['USER_POST_SIM']['is_shell']
        self.pre = foo['USER_PRE_SIM']['command']
        self.is_pre_shell = foo['USER_PRE_SIM']['is_shell']
        self.gcc = foo['RISCV_PREFIX'] + 'gcc'
        self.ld = foo['RISCV_PREFIX'] + 'ld'
        self.root_dir = constants.root
        self.env_dir = foo['USER_ENV_DIR'] + '/'
        self.linker = os.path.join(self.env_dir, foo['USER_LINKER'])
        self.user_abi = foo['USER_ABI'].lower()
        self.user_target = foo['USER_TARGET']
        self.user_sign = foo['USER_SIGN']
        self.objdump = foo['RISCV_PREFIX'] + 'objdump -D '
        self.buildsc = foo['BUILD']
        self.perform_pre = not self.pre is ''
        self.perform_post = not self.post is ''
        self.perform_build = not self.buildsc is ''
        self.compile_cmd = self.gcc+ ' -march={0} -mabi={1} '+compile_flags +\
                ' -T'+self.linker
        self.objdump = foo['RISCV_PREFIX'] + 'objdump -D'
        return sclass

    def initialise(self, suite, work_dir, env):
        self.suite = suite
        self.work_dir = work_dir
        self.compile_cmd = self.compile_cmd + " -I" + env + " -I" + self.env_dir

    def build(self, isa_yaml, platform_yaml):
        ispec = utils.load_yaml(isa_yaml)
        self.isa = ispec["ISA"]
        if self.perform_build:
            logger.debug(self.name + "Build")
            d = dict(isaf=isa_yaml, platformf=platform_yaml, isa=self.isa)
            utils.shellCommand("",
                               Template(self.buildsc).safe_substitute(d)).run()

    def runTests(self, testList):
        for file in testList:
            testentry = testList[file]
            logger.debug(self.name + "Compile")
            test = os.path.join(self.root_dir, str(file))
            test_dir = testentry['work_dir']
            elf = os.path.join(test_dir, str(file.split("/")[-1][:-2]) + '.elf')
            cmd = self.compile_cmd.format(
                map[testentry['isa'].lower()],
                self.user_abi) + ' ' + test + ' -o ' + elf
            execute = cmd + " -D" + " -D".join(testentry['macros'])
            utils.Command(execute).run(cwd=test_dir)
            cmd = self.objdump.format(test, self.user_abi) + ' ' + elf
            utils.shellCommand(
                cmd + ' > {}.disass'.format(str(file.split("/")[-1][:-2]))).run(
                    cwd=test_dir)
            d = dict(elf=elf, testDir=test_dir, isa=self.isa)

            if self.perform_pre:
                logger.debug(self.name + "Pre Sim")
                command = Template(self.pre).safe_substitute(d)
                if self.is_pre_shell:
                    utils.shellCommand(command).run(cwd=test_dir)
                else:
                    utils.Command(command).run(cwd=test_dir)

            logger.debug(self.name + "Simulate")
            command = Template(self.simulator).safe_substitute(d)
            utils.Command(command).run(cwd=test_dir)

            if self.perform_post:
                logger.debug(self.name + "Post Sim")
                command = Template(self.post).safe_substitute(d)
                if self.is_post_shell:
                    utils.shellCommand(command).run(cwd=test_dir)
                else:
                    utils.Command(command).run(cwd=test_dir)
            sign_file = os.path.join(test_dir, self.name[:-1] + ".signature")
            cp = "cat " + os.path.join(test_dir,
                                       self.signature) + " > " + sign_file
            utils.shellCommand(cp, ensure_absolute_paths=True).run()
