import os
import re
import logging
import sys
from string import Template

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger(__name__)

map = {
    'rv32i': 'rv32i',
    'rv32im': 'rv32im',
    'rv32ic': 'rv32ic',
    'rv32ia': 'rv32ia'
}


class riscvOVPsim(pluginTemplate):

    __model__ = "Riscv_OVPsim"
    __version__ = "0.0.1"

    def __init__(self, *args, **kwargs):
        self.simulator = 'riscvOVPsim.exe --variant ${ISA} --program ${elf} --customcontrol --signaturedump --override riscvOVPsim/cpu/sigdump/ResultReg=3 --override riscvOVPsim/cpu/sigdump/SignatureFile=sign --logfile log.txt --override riscvOVPsim/cpu/simulateexceptions=T --override riscvOVPsim/cpu/defaultsemihost=F --override riscvOVPsim/cpu/user_version=${User_Spec_Version} --override riscvOVPsim/cpu/priv_version=${Privilege_Spec_Version} ${misaligned_support} ${mvendorid} ${marchid} ${mimpid}'
        self.signature = 'sign'
        self.pref = 'riscv32-unknown-elf-'
        self.gcc = self.pref + 'gcc'
        self.ld = self.pref + 'ld'
        self.root_dir = constants.root
        self.user_abi = 'ilp32'
        self.user_target = 'riscvOVPsim'
        self.user_sign = 'sign'
        self.objdump = self.pref + 'objdump -D '
        path = os.path.abspath(os.path.dirname(__file__))
        self.isa_spec = os.path.join(path, "ovpsim_isa.yaml")
        self.platform_spec = os.path.join(path, "ovpsim_platform.yaml")
        return super().__init__(*args, **kwargs)

    def initialise(self, suite, work_dir, env):
        compile_flags = ' -static -mcmodel=medany -fvisibility=hidden -nostdlib \
        -nostartfiles '

        self.work_dir = work_dir
        self.suite = suite
        self.env_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                    "env")
        self.linker = os.path.join(self.env_dir, 'link.ld')
        self.compile_cmd = self.gcc+ ' -march={0} -mabi={1} '+compile_flags+' -I'+env+' -I' + self.env_dir +\
                ' -T'+self.linker

    def build(self, isa_yaml, platform_yaml):
        self.simulate_dict = utils.load_yaml(isa_yaml)
        self.simulate_dict.update(utils.load_yaml(platform_yaml))

        #Dirty patch
        if self.simulate_dict['Privilege_Spec_Version'] == 1.1:
            self.simulate_dict['Privilege_Spec_Version'] = "1.10"

        # Misaligned check
        if (self.simulate_dict['hw_data_misaligned_support'] == True):
            self.simulate_dict.update(
                misaligned_support='--override riscvOVPsim/cpu/unaligned=T')
        else:
            self.simulate_dict.update(misaligned_support='')

        # misa
        if (self.simulate_dict['misa']['implemented'] == True):
            self.simulate_dict.update(misa_support='')

        # mvendorid
        if (self.simulate_dict['mvendorid']['implemented'] == False):
            self.simulate_dict.update(mvendorid='')
        else:
            self.simulate_dict.update(
                mvendorid='--override riscvOVPsim/cpu/mvendorid={}'.format(
                    self.simulate_dict['mvendorid']['id']))

        # marchid
        if (self.simulate_dict['marchid']['implemented'] == False):
            self.simulate_dict.update(marchid='')
        else:
            self.simulate_dict.update(
                marchid='--override riscvOVPsim/cpu/marchid={}'.format(
                    self.simulate_dict['marchid']['id']))

        # mimpid
        if (self.simulate_dict['mimpid']['implemented'] == False):
            self.simulate_dict.update(mimpid='')
        else:
            self.simulate_dict.update(
                mimpid='--override riscvOVPsim/cpu/mimpid={}'.format(
                    self.simulate_dict['mimpid']['id']))

        # # mhartids
        # if len(self.simulate_dict['mhartid']) > 1:
        #     sys.exit("This riscvOVPsim model does not support multiple harts")
        # elif self.simulate_dict['mhartid'] != 0:
        #     sys.exit("Atleast one hart id should be 0")

        # mtvec
        #if (self.simulate_dict['mtvec']['BASE']['range']['rangelist'][0] == False):
        #      self.simulate_dict.update(mimpid = '')#self.simulate_dict.update(mimpid = '--override riscvOVPsim/cpu/mimpid=0')
        #else:
        #      self.simulate_dict.update(mimpid = '--override riscvOVPsim/cpu/mimpid={}'.format(self.simulate_dict['mimpid']['id']))

    def simulate(self, file):
        logger.debug(self.name + "Compile")
        test = os.path.join(self.root_dir, str(file))
        test_dir = os.path.join(self.work_dir,
                                str(file.replace(self.suite, '')[:-2]))
        elf = os.path.join(test_dir, str(file.split("/")[-1][:-2]) + '.elf')
        cmd = self.compile_cmd.format(map[isa.lower()],
                                      self.user_abi) + ' ' + test + ' -o ' + elf
        execute = cmd + ' -D' + " -D".join(macros)
        utils.Command(execute).run(cwd=test_dir)
        cmd = self.objdump.format(test, self.user_abi) + ' ' + elf
        utils.shellCommand(
            cmd + ' > {}.disass'.format(str(file.split("/")[-1][:-2]))).run(
                cwd=test_dir)
        test_dir = os.path.join(self.work_dir,
                                str(file.replace(self.suite, '')[:-2]))
        elf = os.path.join(test_dir, str(file.split("/")[-1][:-2]) + '.elf')
        self.simulate_dict.update(elf=elf)
        logger.debug(self.name + "Simulate")
        command = Template(self.simulator).safe_substitute(self.simulate_dict)
        utils.Command(command).run(cwd=test_dir)

        logger.debug(self.name + "Post Sim")
        command = '''cat sign | sed 's/.\{8\}/& /g' | awk '{print $4 " " $3 " " $2 " " $1}' | sed 's/ /\\n/g' > temp; mv temp sign; exit 0'''
        utils.shellCommand(command).run(cwd=test_dir)

        sign_file = os.path.join(
            test_dir, self.name[:-1] + "_" + self.user_target + "_sign")
        cp = "cat " + os.path.join(test_dir, self.signature) + " > " + sign_file
        utils.shellCommand(cp, ensure_absolute_paths=True).run()
        return sign_file

    def runTests(self, testList):
        foo = os.path.join(self.work_dir, "Makefile." + self.name[:-1])
        tlist = 'pmake -j 8 -f ' + foo + " -T ./" + self.name[:-1] + ".log "
        with open(foo, "w") as makefile:
            for file in testList:
                testentry = testList[file]
                target = str(file.split("/")[-1][:-2])
                tlist += target + " "
                makefile.write("\n\n.PHONY : " + target + "\n" + target + " :")
                test = os.path.join(self.root_dir, str(file))
                test_dir = testentry['work_dir']
                elf = os.path.join(test_dir,
                                   str(file.split("/")[-1][:-2]) + '.elf')
                cmd = self.compile_cmd.format(
                    map[testentry['isa'].lower()],
                    self.user_abi) + ' ' + test + ' -o ' + elf
                execute = cmd + ' -D' + " -D".join(testentry['macros'])
                makefile.write("\n\techo \"Running " + target + "\"\n\t" +
                               execute)
                cmd = self.objdump.format(test, self.user_abi) + ' ' + elf
                makefile.write("\n\t" + cmd + ' > {0}/{1}.disass'.format(
                    testentry['work_dir'], str(file.split("/")[-1][:-2])))

                elf = os.path.join(test_dir,
                                   str(file.split("/")[-1][:-2]) + '.elf')
                self.simulate_dict.update(elf=elf)

                command = Template("cd " + testentry['work_dir'] + ';' +
                                   self.simulator + ';').safe_substitute(
                                       self.simulate_dict)
                makefile.write("\n\t" + command)

                command = '''ln -sf ''' + self.env_dir + "/sign_fix.sh ./;sh sign_fix.sh;"
                makefile.write(command)

                sign_file = os.path.join(test_dir,
                                         self.name[:-1] + ".signature")
                cp = "cat " + os.path.join(test_dir, "sign") + " > " + sign_file
                makefile.write("\n\t" + cp)
        utils.shellCommand(tlist).run(cwd=self.work_dir)
