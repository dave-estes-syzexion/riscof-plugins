# riscof-plugins

Contains various model plugins to be used along with RISCOF (https://gitlab.com/incoresemi/riscof)

## riscvOVPsim plugin

```
export PATH=$PATH:/path/to/riscvOVPsim.exe
```
