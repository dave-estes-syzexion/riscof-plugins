### Riscof Plugin for Sail RISC-V formal model

- Using the simulators from the Sail RISC-V formal model

The [Sail RISC-V formal model](https://github.com/rems-project/sail-riscv) generates two
simulators, in C and OCaml.  They can be used as test targets for this compliance suite.

For this purpose, the Sail model needs to be checked out and built on
the machine running the compliance suite.  Follow the build
instructions described the README for building the RV32 and RV64
models.  Once built, please add `$SAIL_RISCV/c_emulator` and
`$SAIL_RISCV/ocaml_emulator` to your path, where $SAIL_RISCV is the
top-level directory containing the model.

- Config file entry
```
DUTPlugin=cSail
#ReferencePlugin=cSail
```

- Export command
```
pwd=pwd;export PYTHONPATH=$pwd:$PYTHONPATH;
```