import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger()

map = {
    'rv32i': 'rv32i',
    'rv32im': 'rv32im',
    'rv32ic': 'rv32ic',
    'rv32ia': 'rv32ia'
}


class grift(pluginTemplate):
    __model__ = "Grift_Sim"
    __version__ = "0.5.0"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)

        path = os.path.abspath(os.path.dirname(__file__))
        self.isa_spec = os.path.join(path, "grift_isa.yaml")
        self.platform_spec = os.path.join(path, "grift_platform.yaml")

        compile_flags = ' -static -mcmodel=medany -fvisibility=hidden -nostdlib \
        -nostartfiles '

        self.pref = "riscv32-unknown-elf-"
        self.gcc = "riscv32-unknown-elf-" + 'gcc'
        self.ld = "riscv32-unknown-elf-" + 'ld'
        self.root_dir = constants.root
        self.env_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                    "env")
        self.linker = os.path.join(self.env_dir, "link.ld")
        self.user_abi = "ilp32"
        self.user_target = "grift"
        self.user_sign = "sign"
        self.objdump = "riscv32-unknown-elf-" + 'objdump -D '
        self.compile_cmd = self.gcc+ ' -march={0} -mabi={1} '+compile_flags +\
                ' -T'+self.linker
        self.objdump = "riscv32-unknown-elf-" + 'objdump -D'
        return sclass

    def initialise(self, suite, work_dir, env):
        self.suite = suite
        self.work_dir = work_dir
        self.compile_cmd = self.compile_cmd + " -I" + env + " -I" + self.env_dir

    def build(self, isa_yaml, platform_yaml):
        ispec = utils.load_yaml(isa_yaml)
        if "C" in ispec["ISA"]:
            self.isa = "RV32GC"
        else:
            self.isa = ispec["ISA"]

    def runTests(self, testList):
        foo = os.path.join(self.work_dir, "Makefile." + self.name[:-1])
        tlist = 'pmake -j 8 -f ' + foo + " -T ./" + self.name[:-1] + ".log "
        with open(foo, "w") as makefile:
            for file in testList:
                testentry = testList[file]
                target = str(file.split("/")[-1][:-2])
                tlist += target + " "
                makefile.write("\n\n.PHONY : " + target + "\n" + target + " :")
                test = os.path.join(self.root_dir, str(file))
                test_dir = testentry['work_dir']
                elf = os.path.join(test_dir,
                                   str(file.split("/")[-1][:-2]) + '.elf')
                cmd = self.compile_cmd.format(
                    map[testentry['isa'].lower()],
                    self.user_abi) + ' ' + test + ' -o ' + elf
                execute = cmd + ' -D' + " -D".join(testentry['macros'])
                makefile.write("\n\techo \"Running " + target + "\"\n\t" +
                               execute)
                cmd = self.objdump.format(test, self.user_abi) + ' ' + elf
                makefile.write("\n\t" + cmd + ' > {0}/{1}.disass'.format(
                    testentry['work_dir'], str(file.split("/")[-1][:-2])))
                elf = os.path.join(test_dir,
                                   str(file.split("/")[-1][:-2]) + '.elf')
                d = dict(elf=elf, testDir=test_dir, isa=self.isa)

                command = Template(
                    "cd " + testentry['work_dir'] +
                    ';grift-sim ${elf} -a ${isa} --halt-pc=grift_halt --mem-dump-begin=begin_signature --mem-dump-end=end_signature > '
                    + self.name[:-1] + '.signature;').safe_substitute(d)
                makefile.write("\n\t" + command)
        utils.shellCommand(tlist).run(cwd=self.work_dir)
