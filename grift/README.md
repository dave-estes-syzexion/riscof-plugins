### Riscof Plugin for Galois RISC-V ISA Formal Tools

- Installation
The steps for installation can be found [here](https://github.com/GaloisInc/grift)

- Config file entry
```
DUTPlugin=grift
#ReferencePlugin=grift
```

- Export command
```
pwd=pwd;export PYTHONPATH=$pwd:$PYTHONPATH;
```